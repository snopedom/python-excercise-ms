FROM python:3.10-alpine

# Install build dependencies missing on Alpine.
RUN apk add --no-cache build-base libffi-dev bash

RUN python -m pip install poetry

# Set working directory inside image.
WORKDIR exercise_ms/

# Insert Poetry files & install deps.
COPY pyproject.toml poetry.lock ./
RUN poetry install --no-dev

# Insert application.
COPY alembic alembic
COPY alembic.ini alembic.ini
COPY exercise_ms exercise_ms
COPY jinja jinja
COPY static static
COPY brief.md brief.md

ENV PORT=8080
EXPOSE 8080

# Add entrypoint & make it executable.
COPY docker-entrypoint.sh docker-entrypoint.sh
RUN chmod a+x docker-entrypoint.sh

ENTRYPOINT ["./docker-entrypoint.sh"]
