# Python Exercise MS

Microservice supporting the Applifting Python candidate exercise. 🚀

The chosen tech stack is standard: FastAPI & Postgres (SQLAlchemy, Alembic & Asyncpg).

Candidates are provided with an assignment brief served as HTML at `/brief`. The brief is rendered from [`brief.md`](brief.md). OpenAPI documentation is available at the usual `/docs` and `/redoc`.

## Development

To run the app locally, first provision a Postgres instance. You can use the provided Compose manifest:

```
docker-compose up -d
```

And install application dependencies:

```
poetry install
```

The application requires configuration via environment variables. See [`exercise_ms.config`](exercise_ms/config.py).

`POSTGRES_URI_MIGRATIONS` is generally the same as the primary URI, but the scheme is `postgresql+asyncpg`.

Once configured, you can apply migrations:

```
poetry run alembic upgrade head
```

And run the API:

```
poetry run task api-dev
```

## Tests

The API is tested to 100% coverage. Tests run against a live Postgres instance, and the testing suite comes with its own Compose:

```
docker-compose -f tests/docker-compose.yml up -d
```

Then run tests:

```
poetry run task test
```

The app will target the testing Postgres instance without any additional configuration. Migrations are applied automatically.

## Lint

Code style is enforced with `black` and `flake8` with some plugins.

```
poetry run task black
poetry run task lint
```

## CI

Tests, lint & black run in GitLab CI automatically.

Black is used as a linter via the `--check` flag - it exits with non-0 code if it finds code that it wants to reformat.

Note that `tests/docker-compose.yml` is not used in CI. GitLab already runs the pipeline in a container, and provides an easy way to provision a Postgres service. This is special-cased during test setup via the `TESTING_CI` env var.

## Deployment

[`Dockerfile`](Dockerfile) is provided for deployment. The entry-point is a [bash script](docker-entrypoint.sh).

To run database migrations, invoke it with `migrate` as the first arg. To run the API, pass `api`.

Use the `PORT` environment variable to configure which port the API listens on.
