import asyncio
from logging.config import fileConfig

from alembic import context
from sqlalchemy import engine_from_config, pool
from sqlalchemy.engine import Connection
from sqlalchemy.ext.asyncio import AsyncEngine

from exercise_ms.config import Settings
from exercise_ms.database.schema._base import Base

config = context.config

if config.config_file_name is not None:
    fileConfig(config.config_file_name)

target_metadata = Base.metadata


def _get_migration_settings() -> Settings:
    """
    Resolve settings to use for migrations.

    If settings are provided via the context (e.g. when running tests), use them.
    Otherwise, load settings from the environment.
    """
    context_settings: Settings | None = config.attributes.get("settings")
    if context_settings is not None:
        return context_settings
    return Settings()


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode."""
    settings = _get_migration_settings()
    context.configure(
        url=settings.POSTGRES_URI_MIGRATIONS,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def do_run_migrations(connection: Connection) -> None:
    context.configure(connection=connection, target_metadata=target_metadata)

    with context.begin_transaction():
        context.run_migrations()


async def run_migrations_online() -> None:
    """Run migrations in 'online' mode."""
    settings = _get_migration_settings()
    sync_engine = engine_from_config(
        config.get_section(config.config_ini_section),
        url=settings.POSTGRES_URI_MIGRATIONS,
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
        future=True,
    )
    connectable = AsyncEngine(sync_engine)

    async with connectable.connect() as connection:
        await connection.run_sync(do_run_migrations)

    await connectable.dispose()


if context.is_offline_mode():
    run_migrations_offline()
else:
    asyncio.run(run_migrations_online())
