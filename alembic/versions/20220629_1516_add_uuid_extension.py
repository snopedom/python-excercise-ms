"""add uuid extension

Created: 2022-06-29 15:16:01.265260+00:00
"""

from alembic import op

# Revision identifiers, used by Alembic.
revision = 'f71147c99276'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.execute('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')


def downgrade():
    op.execute('DROP EXTENSION "uuid-ossp";')
