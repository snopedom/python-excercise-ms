#!/bin/bash

set -e

if [ "$1" == 'api' ]; then
  echo "Entry-point: API"
  exec poetry run uvicorn exercise_ms.api.app:make_app --factory --host=0.0.0.0 --port="${PORT:-5000}" --workers 1 --proxy-headers
elif [ "$1" == 'migrate' ]; then
  echo "Entry-point: migrations"
  exec poetry run alembic upgrade head
else
  echo "Entry-point: unknown command"
fi
