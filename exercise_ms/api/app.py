import logging
from pathlib import Path

import sentry_sdk
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from exercise_ms.api.brief import router_brief
from exercise_ms.api.v1.app import router_v1
from exercise_ms.config import Settings
from exercise_ms.container import AppContainer
from exercise_ms.database.connection import get_database

log = logging.getLogger(__name__)


def _init_sentry(settings: Settings) -> None:
    """If Sentry DSN is configured, init the SDK."""
    if settings.SENTRY_DSN is None:
        return
    sentry_sdk.init(dsn=settings.SENTRY_DSN)


def _production_init() -> AppContainer:
    """Construct a production container."""
    prod_settings = Settings()
    _init_sentry(prod_settings)
    database = get_database(prod_settings)
    return AppContainer(database, prod_settings)


def make_app(container: AppContainer | None = None) -> FastAPI:
    """Construct API instance."""
    if container is None:
        container = _production_init()

    app = FastAPI(
        title="Python exercise",
        description="Offers microservice.",
        version="1",
    )
    app.state.container = container

    app.mount("/static", StaticFiles(directory=Path("static")), name="static")

    app.include_router(router_v1, prefix="/api/v1")
    app.include_router(router_brief, include_in_schema=False)

    @app.on_event("startup")
    async def on_startup() -> None:
        """Async init."""
        log.info("API startup")
        await container.startup()

    @app.on_event("shutdown")
    async def on_shutdown() -> None:
        """Async cleanup."""
        log.info("API shutdown")
        await container.shutdown()

    return app
