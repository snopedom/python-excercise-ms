from pathlib import Path

import markdown2
from fastapi import APIRouter, Request
from fastapi.responses import HTMLResponse, Response
from fastapi.templating import Jinja2Templates

router_brief = APIRouter()

templates = Jinja2Templates(Path("jinja"))


def _render_content() -> str:
    md_path = Path("brief.md")
    m = markdown2.markdown_path(md_path)
    return str(m)


@router_brief.get("/", response_class=HTMLResponse)
def get_brief(request: Request) -> Response:
    """Get the assignment brief in HTML form."""
    context = {
        "request": request,  # Required by Starlette.
        "brief_content": _render_content(),
    }
    return templates.TemplateResponse("brief.html", context)
