import typing as t
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, Header

from exercise_ms.api.v1.domain import (
    AuthResponse,
    OfferResponse,
    OffersResponse,
    RegisterProductRequest,
    RegisterProductResponse,
)
from exercise_ms.container import AppContainer, request_container
from exercise_ms.repository.product import ProductExistsError

router_v1 = APIRouter()


@router_v1.post(
    "/auth",
    response_model=AuthResponse,
    status_code=201,
)
async def auth(container: AppContainer = Depends(request_container)) -> AuthResponse:
    """
    Generate access token for a new session.

    Authenticate requests by including a header: `Bearer: <access-token>`.
    """
    session_service = container.session_service()
    session = await session_service.create()
    return AuthResponse(access_token=session.id)


async def _get_session_id(
    auth_header: t.Optional[str] = Header(
        None,
        alias="Bearer",
        description="Access token from the auth endpoint.",
    ),
    container: AppContainer = Depends(request_container),
) -> UUID:
    """If req is properly authenticated, return the access token."""
    if auth_header is None:
        raise HTTPException(401, "Access token missing")

    try:
        access_token = UUID(auth_header)
    except (ValueError, TypeError):
        raise HTTPException(401, "Access token invalid")

    session_service = container.session_service()
    session_id = await session_service.get_session_id(access_token)

    if session_id is None:
        raise HTTPException(401, "Access token invalid")

    return session_id


@router_v1.post(
    "/products/register",
    response_model=RegisterProductResponse,
    status_code=201,
    responses={
        401: {"description": "Bad authentication"},
        409: {"description": "Product ID already registered"},
        422: {"description": "Bad request data"},
    },
)
async def register_product(
    data: RegisterProductRequest,
    session_id: UUID = Depends(_get_session_id),
    container: AppContainer = Depends(request_container),
) -> RegisterProductResponse:
    """Register a new product."""
    product_service = container.product_service()
    try:
        product = await product_service.create(session_id, data.id, data.name, data.description)
    except ProductExistsError:
        raise HTTPException(409, "Product already exists")
    return RegisterProductResponse(id=product.client_side_id)


@router_v1.get(
    "/products/{product_id}/offers",
    response_model=OffersResponse,
    status_code=200,
    responses={
        401: {"description": "Bad authentication"},
        404: {"description": "Product ID has not been registered"},
        422: {"description": "Bad request data"},
    },
)
async def get_offers(
    product_id: UUID,
    session_id: UUID = Depends(_get_session_id),
    container: AppContainer = Depends(request_container),
) -> OffersResponse:
    """Get offers for an existing product by ID."""
    product_service = container.product_service()
    product = await product_service.get(session_id, client_side_id=product_id)

    if product is None:
        raise HTTPException(404, "Product does not exist")

    offer_service = container.offer_service()
    offers = offer_service.generate_offers(product.id)

    return [
        OfferResponse(
            id=offer.id,
            price=offer.price,
            items_in_stock=offer.items_in_stock,
        )
        for offer in offers
    ]
