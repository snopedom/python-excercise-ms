from uuid import UUID

from pydantic import BaseModel


class AuthResponse(BaseModel):
    access_token: UUID


class RegisterProductRequest(BaseModel):
    id: UUID
    name: str
    description: str


class RegisterProductResponse(BaseModel):
    id: UUID


class OfferResponse(BaseModel):
    id: UUID
    price: int
    items_in_stock: int


OffersResponse = list[OfferResponse]
