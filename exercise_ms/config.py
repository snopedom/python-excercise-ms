from enum import Enum

from pydantic import AnyHttpUrl, BaseSettings, PostgresDsn


class Env(Enum):
    PRODUCTION = "production"
    DEVELOPMENT = "development"
    TESTING = "testing"


class Settings(BaseSettings):
    """Application configuration."""

    ENVIRONMENT: Env

    POSTGRES_URI: PostgresDsn
    POSTGRES_URI_MIGRATIONS: PostgresDsn

    SENTRY_DSN: AnyHttpUrl | None = None
