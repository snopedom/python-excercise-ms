from functools import partial

from databases import Database
from fastapi import Request

from exercise_ms.config import Settings
from exercise_ms.database.uow import UowFactory, make_uow
from exercise_ms.repository.product import ProductRepository
from exercise_ms.repository.session import SessionRepository
from exercise_ms.service.offer import OfferService
from exercise_ms.service.product import ProductService
from exercise_ms.service.session import SessionService


class AppContainer:
    """Dependency container."""

    def __init__(self, database: Database, settings: Settings) -> None:
        self._database = database
        self._settings = settings

    async def startup(self) -> None:
        """Async init."""
        await self._database.connect()

    async def shutdown(self) -> None:
        """Async cleanup."""
        await self._database.disconnect()

    def uow_factory(self) -> UowFactory:
        return partial(make_uow, self._database)

    def session_service(self) -> SessionService:
        return SessionService(self.uow_factory(), SessionRepository())

    def product_service(self) -> ProductService:
        return ProductService(self.uow_factory(), ProductRepository())

    def offer_service(self) -> OfferService:
        return OfferService()


def request_container(req: Request) -> AppContainer:
    container = getattr(req.app.state, "container", None)
    assert isinstance(container, AppContainer)
    return container
