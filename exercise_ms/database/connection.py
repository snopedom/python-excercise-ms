from databases import Database

from exercise_ms.config import Settings


def get_database(settings: Settings) -> Database:
    """Instantiate DB connection pool abstraction."""
    return Database(
        url=settings.POSTGRES_URI,
        min_size=5,
        max_size=10,
        server_settings={"application_name": "python/exercise-ms"},
    )
