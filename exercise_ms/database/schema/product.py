from sqlalchemy import Column, ForeignKey, Index, Table, Text, text
from sqlalchemy_utils import UUIDType

from exercise_ms.database.schema._base import Base

Product = Table(
    "product",
    Base.metadata,
    Column(
        "id",
        UUIDType,
        primary_key=True,
        server_default=text("uuid_generate_v4()"),
    ),
    Column("session_id", UUIDType, ForeignKey("session.id", ondelete="CASCADE")),
    Column("client_side_id", UUIDType, nullable=False),  # ID given by the user.
    Column("name", Text, nullable=False),
    Column("description", Text, nullable=False),
    Index("product_session_ix", "session_id", "client_side_id", unique=True),
)
