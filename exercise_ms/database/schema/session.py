from sqlalchemy import Column, Table, text
from sqlalchemy_utils import ArrowType, UUIDType

from exercise_ms.database.schema._base import Base

Session = Table(
    "session",
    Base.metadata,
    Column(
        "id",
        UUIDType,
        primary_key=True,
        server_default=text("uuid_generate_v4()"),
    ),
    Column(
        "created_at",
        ArrowType,
        nullable=False,
        server_default=text("now()"),
    ),
)
