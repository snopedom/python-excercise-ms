import logging
import typing as t
from contextlib import asynccontextmanager

from databases.backends.postgres import Record
from databases.core import Connection, Database
from sqlalchemy.sql import ClauseElement

log = logging.getLogger(__name__)

T = t.TypeVar("T")
Query = t.Union[ClauseElement, str]


class Uow:
    """Unit of work."""

    def __init__(self, conn: Connection) -> None:
        self._conn = conn

    async def fetch_one(self, query: Query, record_type: t.Type[T]) -> T | None:
        """Fetch single record, if it exists."""
        record: Record | None = await self._conn.fetch_one(query)
        if record is None:
            return None
        return record_type(**record)

    async def fetch_many(self, query: Query, record_type: t.Type[T]) -> list[T]:
        """Fetch multiple records."""
        records: list[Record] = await self._conn.fetch_all(query)
        return [record_type(**record) for record in records]

    async def execute_one(self, query: Query) -> None:
        """Execute single query."""
        await self._conn.execute(query)


@asynccontextmanager
async def make_uow(database: Database) -> t.AsyncContextManager[Uow]:
    """Create & manage a unit of work instance."""
    async with database.connection() as conn:
        async with conn.transaction():
            yield Uow(conn)


UowFactory = t.Callable[[], t.AsyncContextManager[Uow]]
