from dataclasses import dataclass
from uuid import UUID


@dataclass
class OfferData:
    id: UUID
    price: int
    items_in_stock: int
