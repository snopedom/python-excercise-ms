from dataclasses import dataclass
from uuid import UUID

from asyncpg.exceptions import UniqueViolationError
from sqlalchemy import select
from sqlalchemy.dialects.postgresql import insert

from exercise_ms.database.schema.product import Product
from exercise_ms.database.uow import Uow


class ProductExistsError(RuntimeError):
    ...


@dataclass
class ProductData:
    id: UUID
    session_id: UUID
    client_side_id: UUID
    name: str
    description: str


class ProductRepository:
    table = Product

    async def get(self, uow: Uow, session_id: UUID, client_side_id: UUID) -> ProductData | None:
        query = select(self.table)
        query = query.where(self.table.c.session_id == session_id)
        query = query.where(self.table.c.client_side_id == client_side_id)
        return await uow.fetch_one(query, ProductData)

    async def create(
        self,
        uow: Uow,
        session_id: UUID,
        client_side_id: UUID,
        name: str,
        description: str,
    ) -> ProductData:
        data = {
            self.table.c.session_id: session_id,
            self.table.c.client_side_id: client_side_id,
            self.table.c.name: name,
            self.table.c.description: description,
        }
        query = insert(self.table).values(data).returning(self.table)
        try:
            return await uow.fetch_one(query, ProductData)
        except UniqueViolationError:
            raise ProductExistsError
