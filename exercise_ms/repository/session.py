from dataclasses import dataclass
from uuid import UUID

from arrow import Arrow
from sqlalchemy import select
from sqlalchemy.dialects.postgresql import insert

from exercise_ms.database.schema.session import Session
from exercise_ms.database.uow import Uow


@dataclass
class SessionData:
    id: UUID
    created_at: Arrow


class SessionRepository:
    table = Session

    async def get(self, uow: Uow, id_: UUID) -> SessionData | None:
        query = select(self.table).where(self.table.c.id == id_)
        return await uow.fetch_one(query, SessionData)

    async def create(self, uow: Uow) -> SessionData:
        query = insert(self.table).values().returning(self.table)
        return await uow.fetch_one(query, SessionData)
