from dataclasses import dataclass
from datetime import timedelta
from random import Random
from uuid import UUID

from arrow import Arrow

from exercise_ms.repository.offer import OfferData


def _floor_to_minute(time: Arrow) -> Arrow:
    return time.replace(second=0, microsecond=0)


def _floor_to_hour(time: Arrow) -> Arrow:
    return time.replace(minute=0, second=0, microsecond=0)


def _interpolate(time_opn: Arrow, opn: int, time_cls: Arrow, cls: int, time_now: Arrow) -> int:
    """Determine value between two points in time."""
    assert time_opn <= time_now <= time_cls

    lifetime = time_cls.int_timestamp - time_opn.int_timestamp
    progress = time_now.int_timestamp - time_opn.int_timestamp

    progress_pct = progress / lifetime
    remaining_pct = 1 - progress_pct

    assert 0 <= progress_pct <= 1
    assert 0 <= remaining_pct <= 1

    return int(opn * remaining_pct + cls * progress_pct)


@dataclass
class _OfferDefinition:
    id: UUID
    time_open: Arrow
    time_close: Arrow
    price_open: int
    price_close: int
    items_open: int
    items_close: int

    def price_at(self, now: Arrow) -> int:
        """Interpolate price at given time."""
        return _interpolate(self.time_open, self.price_open, self.time_close, self.price_close, now)

    def items_at(self, now: Arrow) -> int:
        """Interpolate items in stock at given time."""
        return _interpolate(self.time_open, self.items_open, self.time_close, self.items_close, now)


class OfferService:
    """Provide stateless & deterministic offer generation."""

    def _define_offer(
        self,
        product_id: UUID,
        product_price: int,
        time_opn: Arrow,
        time_cls: Arrow,
    ) -> _OfferDefinition:
        """
        Define offer for a product.

        Definitions are aware of the offers open & close time, and can calculate price
        and items in stock at a given point in their lifespan.
        """
        seed = product_id.int + time_opn.int_timestamp
        rng = Random(seed)

        uuid = UUID(bytes=rng.randbytes(16))
        max_deviation = int(product_price * 0.1)  # 10% of price is maximum offer deviation.

        def price_rng() -> int:
            return product_price + rng.randint(-max_deviation, max_deviation)

        return _OfferDefinition(
            id=uuid,
            time_open=time_opn,
            time_close=time_cls,
            price_open=price_rng(),
            price_close=price_rng(),
            items_open=rng.randint(100, 1_000),
            items_close=0,  # Items always converge towards 0 - sold out.
        )

    def generate_offers(self, product_id: UUID, time: Arrow | None = None) -> list[OfferData]:
        """
        Generate offers for a product ID at some point in time.

        Despite randomness, generation is entirely deterministic & repeatable. The RNG is seeded
        on both parameters. In addition, the generator conforms to the following rules.

        * Given the same product ID, the same amount of offers is generated always.
        * Each hour, one offer disappears, and a new one appears.
        * Each minute, the prices for existing offers update.
        * Each offer has a random open & close price.
        * Over the offer's lifetime, the price moves from the open towards the close.
        * Each offer has a random initial amount of items in stock.
        * Over the offer's lifetime, the amount of items converges towards 0.

        Time is only parametrised for repeatable testing. In production, use the default.
        """
        if time is None:
            time = Arrow.utcnow()

        product_rng = Random(product_id.bytes)  # Init product generator.

        n_offers = product_rng.randint(3, 10)  # Generate 3 to 10 offers.
        lifetime = timedelta(hours=n_offers)  # Offers expire after this amount of time.

        product_price = product_rng.randint(1_000, 100_000)  # Offers revolve around this price.

        offer_gen_time = _floor_to_hour(time)  # First offer open time.
        price_time = _floor_to_minute(time)  # We want prices for this time.

        generated_offers: list[OfferData] = []

        for hour_diff in range(n_offers):

            # Times at which this offer opens & closes.
            time_opn = offer_gen_time - timedelta(hours=hour_diff)
            time_cls = time_opn + lifetime

            # First generate the offer definition, which spans over a period of time.
            # This is needed to acquire the current price.
            offer_definition = self._define_offer(
                product_id=product_id,
                product_price=product_price,
                time_opn=time_opn,
                time_cls=time_cls,
            )

            # Now determine price & items at this point in time.
            offer = OfferData(
                id=offer_definition.id,
                price=offer_definition.price_at(price_time),
                items_in_stock=offer_definition.items_at(price_time),
            )
            generated_offers.append(offer)

        return generated_offers
