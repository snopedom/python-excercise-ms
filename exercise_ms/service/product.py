from uuid import UUID

from exercise_ms.database.uow import UowFactory
from exercise_ms.repository.product import ProductData, ProductRepository


class ProductService:
    def __init__(self, uow_factory: UowFactory, repository: ProductRepository) -> None:
        self._uow_factory = uow_factory
        self._repository = repository

    async def get(self, session_id: UUID, client_side_id: UUID) -> ProductData | None:
        async with self._uow_factory() as uow:
            return await self._repository.get(uow, session_id, client_side_id)

    async def create(
        self,
        session_id: UUID,
        client_side_id: UUID,
        name: str,
        description: str,
    ) -> ProductData:
        """Register a new product."""
        async with self._uow_factory() as uow:
            return await self._repository.create(
                uow=uow,
                session_id=session_id,
                client_side_id=client_side_id,
                name=name,
                description=description,
            )
