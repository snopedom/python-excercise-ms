from uuid import UUID

from exercise_ms.database.uow import UowFactory
from exercise_ms.repository.session import SessionData, SessionRepository


class SessionService:
    def __init__(self, uow_factory: UowFactory, repository: SessionRepository) -> None:
        self._uow_factory = uow_factory
        self._repository = repository

    async def get_session_id(self, access_token: UUID) -> UUID | None:
        async with self._uow_factory() as uow:
            data = await self._repository.get(uow, access_token)
        if not data:
            return
        return data.id

    async def create(self) -> SessionData:
        async with self._uow_factory() as uow:
            return await self._repository.create(uow)
