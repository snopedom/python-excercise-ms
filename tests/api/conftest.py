import pytest
from fastapi import FastAPI
from httpx import AsyncClient


@pytest.fixture
async def api_http(managed_app: FastAPI) -> AsyncClient:
    """Provide HTTP client for making calls into the base API."""
    async with AsyncClient(app=managed_app, base_url="http://test") as http:
        yield http
