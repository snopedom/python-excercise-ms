from httpx import AsyncClient


async def test_brief(api_http: AsyncClient):
    resp = await api_http.get("/")
    assert resp.status_code == 200


async def test_brief_styles(api_http: AsyncClient):
    resp = await api_http.get("/static/styles.css")
    assert resp.status_code == 200
