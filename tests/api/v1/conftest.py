from uuid import UUID

import pytest
from fastapi import FastAPI
from httpx import AsyncClient

from exercise_ms.container import AppContainer


@pytest.fixture
async def api_v1_http(managed_app: FastAPI) -> AsyncClient:
    """Provide HTTP client for making calls into the v1 API."""
    async with AsyncClient(app=managed_app, base_url="http://test/api/v1") as http:
        yield http


@pytest.fixture
async def session_id(container: AppContainer) -> UUID:
    """Establish session & return ID."""
    service = container.session_service()
    session = await service.create()
    return session.id


@pytest.fixture
async def authenticated_http(session_id: UUID, api_v1_http: AsyncClient) -> AsyncClient:
    """Authenticate the v1 API HTTP client."""
    api_v1_http.headers.update({"Bearer": str(session_id)})
    return api_v1_http


@pytest.fixture
async def product_id(container: AppContainer, session_id: UUID) -> UUID:
    """Inject a product & return its ID."""
    service = container.product_service()
    product = await service.create(
        session_id,
        UUID("9827f849-d313-4a4d-819f-345169a1401b"),
        "product-name",
        "product-description",
    )
    return product.client_side_id
