from uuid import UUID

import pytest
from httpx import AsyncClient


async def test_auth(api_v1_http: AsyncClient):
    resp = await api_v1_http.post("/auth")
    assert resp.status_code == 201
    assert "access_token" in resp.json()


async def test_register_product_no_auth(api_v1_http: AsyncClient):
    resp = await api_v1_http.post("/products/register")
    assert resp.status_code == 401


async def test_register_product_bad_auth(api_v1_http: AsyncClient):
    resp = await api_v1_http.post("/products/register", headers={"Bearer": "not-uuid"})
    assert resp.status_code == 401


async def test_register_product_session_does_not_exist(api_v1_http: AsyncClient):
    bad_access_token = UUID("167350ff-aa28-4f41-bbfc-a1b4fcae8781")
    resp = await api_v1_http.post("/products/register", headers={"Bearer": str(bad_access_token)})
    assert resp.status_code == 401


@pytest.mark.parametrize("data", [
    {"id": "no-uuid"},
    {"id": "7d582d4d-62ea-4771-bb59-1c8ce19183e9"},
    {"name": "name", "description": "description"},
    {"id": "no-uuid", "name": "name", "description": "description"},
])
async def test_register_product_bad_data(authenticated_http: AsyncClient, data: dict):
    resp = await authenticated_http.post("/products/register", json=data)
    assert resp.status_code == 422


async def test_register_product_ok(authenticated_http: AsyncClient):
    product_id = UUID("0d930484-f43f-4168-ac6b-5916afbeb89c")
    data = {"id": str(product_id), "name": "product-name", "description": "product-description"}
    resp = await authenticated_http.post("/products/register", json=data)
    assert resp.status_code == 201
    assert UUID(resp.json()["id"]) == product_id


async def test_register_product_conflict(authenticated_http: AsyncClient):
    product_id = UUID("0d930484-f43f-4168-ac6b-5916afbeb89c")
    data = {"id": str(product_id), "name": "product-name", "description": "product-description"}
    resp_1 = await authenticated_http.post("/products/register", json=data)
    resp_2 = await authenticated_http.post("/products/register", json=data)
    assert resp_1.status_code == 201
    assert resp_2.status_code == 409


async def test_offers_no_auth(api_v1_http: AsyncClient):
    product_id = UUID("25ebb4e4-2784-4a5b-a86d-a58442108e5b")
    resp = await api_v1_http.get(f"products/{product_id}/offers")
    assert resp.status_code == 401


async def test_offers_product_not_exists(authenticated_http: AsyncClient):
    product_id = UUID("25ebb4e4-2784-4a5b-a86d-a58442108e5b")
    resp = await authenticated_http.get(f"products/{product_id}/offers")
    assert resp.status_code == 404


async def test_offers_product_bad_id(authenticated_http: AsyncClient):
    product_id = "not-uuid"
    resp = await authenticated_http.get(f"products/{product_id}/offers")
    assert resp.status_code == 422


async def test_offers_ok(authenticated_http: AsyncClient, product_id: UUID):
    resp = await authenticated_http.get(f"products/{product_id}/offers")
    assert resp.status_code == 200
    data = resp.json()
    assert isinstance(data, list)
    assert len(data) > 0
