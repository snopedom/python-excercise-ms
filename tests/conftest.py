import asyncio
from os import environ

import pytest
from alembic import command as alembic_command
from alembic.config import Config as AlembicConfig
from asgi_lifespan import LifespanManager
from asyncpg.exceptions import UndefinedTableError
from sqlalchemy import delete, select

from exercise_ms.api.app import make_app
from exercise_ms.config import Env, Settings
from exercise_ms.container import AppContainer
from exercise_ms.database.connection import get_database
from exercise_ms.database.schema._base import Base


def _settings_ci() -> Settings:
    """Get settings for a CI testing session."""
    return Settings(
        ENVIRONMENT=Env.TESTING,
        POSTGRES_URI="postgresql://pyex-test:pyex-test@postgres:5432/pyex-test",
        POSTGRES_URI_MIGRATIONS="postgresql+asyncpg://pyex-test:pyex-test@postgres:5432/pyex-test",
        SENTRY_DSN=None,
    )


def _settings_local() -> Settings:
    """Get settings for a local testing session."""
    return Settings(
        ENVIRONMENT=Env.TESTING,
        POSTGRES_URI="postgresql://pyex-test:pyex-test@localhost:7501/pyex-test",
        POSTGRES_URI_MIGRATIONS="postgresql+asyncpg://pyex-test:pyex-test@localhost:7501/pyex-test",
        SENTRY_DSN=None,
    )


@pytest.fixture(scope="session")
def settings() -> Settings:
    """Overridden settings for the session."""
    ci = environ.get("TESTING_CI")
    return _settings_ci() if ci is not None else _settings_local()


async def _find_tables_with_data(settings: Settings) -> list[str]:
    """
    Find names of tables that contain at least 1 row.

    Propagate errors other than the table not being defined, which is ok.
    """
    table_names: list[str] = []

    # We are at session scope - app is not yet running. We need to provision
    # our own connection to run this query.
    database = get_database(settings)
    await database.connect()

    for table in Base.metadata.sorted_tables:
        select_query = select(table)
        try:
            record = await database.fetch_one(select_query)
        except UndefinedTableError:
            continue  # Undefined table is ok - means migrations did not run yet.
        if record is None:
            continue  # Table is empty - we like to see this.
        table_names.append(table.name)

    await database.disconnect()  # Dispose connections before temporary loop closes.
    return table_names


@pytest.fixture(scope="session", autouse=True)
def ensure_no_data_in_database(settings):
    """
    Prevent tests from running if there is data in the database.

    Because this is a session-scoped fixture, the function-scoped event loop fixture is not
    yet available. We need to do async IO, so we spawn our own temporary loop.
    """
    try:
        non_empty_tables: list[str] = asyncio.run(_find_tables_with_data(settings))
    except Exception as exc:
        pytest.exit(f"Cannot verify that database is empty before running tests: {exc}")
    else:
        if non_empty_tables:
            pytest.exit("Database is not empty; tests will not run to prevent data loss")


@pytest.fixture(scope="session", autouse=True)
def ensure_migrations(settings, ensure_no_data_in_database):
    """Run migrations before the session begins."""
    config = AlembicConfig("alembic.ini")
    config.attributes["settings"] = settings
    alembic_command.upgrade(config, revision="head")


@pytest.fixture
def container(settings) -> AppContainer:
    """
    Provide function-bound container.

    This fixture does not call the container's startup hook!
    """
    database = get_database(settings)
    return AppContainer(database, settings)


@pytest.fixture
async def managed_app(container):
    """Orchestrate API startup and shutdown events."""
    app = make_app(container)
    async with LifespanManager(app):
        yield app


@pytest.fixture(autouse=True)
async def database_cleanup(container, managed_app):
    """Ensure no data persists between tests."""
    yield  # Let test run.
    for table in reversed(Base.metadata.sorted_tables):
        await container._database.execute(delete(table))
