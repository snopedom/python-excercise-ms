from datetime import timedelta
from pathlib import Path
from uuid import UUID

import pytest
from arrow import Arrow

from exercise_ms.container import AppContainer
from exercise_ms.service.offer import OfferService


@pytest.fixture
def service(container: AppContainer) -> OfferService:
    return container.offer_service()


def _read_uuids() -> list[UUID]:
    """Read stored UUIDs from disk & return instances."""
    text = Path("tests", "uuids.txt").read_text()
    uuids: list[UUID] = []
    for uuid_string in text.split():
        uuid = UUID(uuid_string)
        uuids.append(uuid)
    return uuids


_uuids = _read_uuids()


@pytest.mark.parametrize("uuid", _uuids)
@pytest.mark.parametrize("time", [Arrow(2020, 1, 1), Arrow(1995, 7, 5), Arrow(2031, 12, 24)])
def test_generate_offers_determinism(service: OfferService, uuid: UUID, time: Arrow):
    """Generator gives deterministic results."""
    offers_a = service.generate_offers(product_id=uuid, time=time)
    offers_b = service.generate_offers(product_id=uuid, time=time)

    assert offers_a == offers_b
    assert len(offers_a) > 0


@pytest.mark.parametrize("uuid", _uuids)
def test_generate_offers_within_hour(service: OfferService, uuid: UUID):
    """Generated offers remain consistent within an hour."""
    first_minute = Arrow(2021, 1, 1)
    times_to_check = [first_minute + timedelta(minutes=m) for m in range(1, 59)]

    first_minute_offers = service.generate_offers(uuid, first_minute)
    first_minute_ids = set(offer.id for offer in first_minute_offers)

    for time in times_to_check:
        offers_now = service.generate_offers(uuid, time)
        assert first_minute_ids == set(offer.id for offer in offers_now)


@pytest.mark.parametrize("uuid", _uuids)
def test_generate_offers_across_hour(service: OfferService, uuid: UUID):
    """Exactly one offer is rotated each hour."""
    first_hour = Arrow(2021, 1, 1)
    times_to_check = [first_hour + timedelta(hours=h) for h in range(1, 100)]

    first_hour_offers = service.generate_offers(uuid, first_hour)
    n_offers = len(first_hour_offers)  # Svc should always generate this many offers.
    previous_offer_ids = set(offer.id for offer in first_hour_offers)

    for time in times_to_check:
        offers_now = service.generate_offers(uuid, time)
        ids_now = set(offer.id for offer in offers_now)
        assert previous_offer_ids != ids_now  # Not exact same IDs.

        common_ids = previous_offer_ids & ids_now  # IDs in both hours.
        assert len(common_ids) == n_offers - 1  # Exactly 1 is different.

        previous_offer_ids = ids_now
